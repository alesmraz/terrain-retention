export const COLUMNS_COUNT_MIN = 1
export const COLUMNS_COUNT_MAX = 10
export const ROWS_COUNT_MIN = 1
export const ROWS_COUNT_MAX = 5
