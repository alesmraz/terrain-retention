export function calculateRetention(columns: number[]) {
  const size = columns.length

  let sum = 0

  for (let i = 1; i < size - 1; i++) {
    let left = columns[i]

    for (let j = 0; j < i; j++) {
      left = Math.max(left, columns[j])
    }

    let right = columns[i]

    for (let j = i + 1; j < size; j++) {
      right = Math.max(right, columns[j])
    }

    sum += Math.min(left, right) - columns[i]
  }

  return sum
}

export default function useTerrainRetention(columns: number[]) {
  const water = columns.map((columns, index, list) => {
    if (index === 0 || index === list.length - 1) {
      return 0
    }

    const leftWall = list.slice(0, index).reduce((a, b) => Math.max(a, b))
    const rightWall = list.slice(index).reduce((a, b) => Math.max(a, b))
    const maxWater = Math.min(leftWall, rightWall)
    return Math.max(maxWater - columns, 0)
  })

  const data = columns.map((level, idx) => ({
    ground: Array.from({ length: level }, (_, key) => key),
    water: Array.from({ length: water[idx] }, (_, key) => key),
  }))

  // NOTE: This can be used to calc total water retention too, but alg. upper is more academic
  // const waterRetentionTotal = water.reduce((a,b) => a+b)

  return { sum: calculateRetention(columns), data }
}
