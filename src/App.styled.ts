import styled from "@emotion/styled"

export const SContainer = styled.main`
  background-color: #fff;
  justify-content: center;
  align-items: center;
  width: 500px;
  margin: 10px;
  border-radius: 10px;
  padding: 20px;
  box-shadow: 4px 4px 9px #00000080;
`

export const SBoard = styled.div`
  display: flex;
  align-items: flex-end;
`

export const SResult = styled.h4`
  text-align: center;
`
