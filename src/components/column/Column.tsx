import { PropsWithChildren } from "react"
import { ROWS_COUNT_MAX, ROWS_COUNT_MIN } from "../../limits"
import { ColumnControl } from "../columnControl/ColumnControl"
import { SCol, SContainer } from "./Column.styled"

interface Props {
  columns: number[]
  setColumns: (value: number[]) => void
  currentColumnIndex: number
}

export function Column(props: PropsWithChildren<Props>) {
  const { columns, currentColumnIndex, setColumns, children } = props

  const colRowsCount = columns[currentColumnIndex]

  function onRowAdd() {
    if (colRowsCount < ROWS_COUNT_MAX) {
      const newColumns = [...columns]
      newColumns[currentColumnIndex] = colRowsCount + 1
      setColumns(newColumns)
    }
  }

  function onRowRemove() {
    if (colRowsCount > ROWS_COUNT_MIN) {
      const newColumns = [...columns]
      newColumns[currentColumnIndex] = colRowsCount - 1
      setColumns(newColumns)
    }
  }

  return (
    <SCol>
      <ColumnControl
        isVisible={colRowsCount === 0}
        onRowAdd={onRowAdd}
        onRowRemove={onRowRemove}
      />
      <SContainer>{children}</SContainer>
    </SCol>
  )
}
