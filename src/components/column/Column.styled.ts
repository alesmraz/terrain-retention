import styled from "@emotion/styled"

export const SCol = styled.div`
  width: 50px;
`

export const SContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`
