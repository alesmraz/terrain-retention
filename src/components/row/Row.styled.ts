import styled from "@emotion/styled"

export const SRow = styled.div<{ variant: "water" | "ground" }>`
  border: 1px solid #e7e7e7;
  height: 48px;
  width: 100%;
  background-color: ${({ variant }) =>
    variant === "water" ? "#3f51b5" : "#795548"};
`
