import { SRow } from "./Row.styled"

interface Props {
  variant: "water" | "ground"
}

export function Row(props: Props) {
  return <SRow variant={props.variant} />
}
