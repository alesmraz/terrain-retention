import { css } from "@emotion/react"
import styled from "@emotion/styled"

export const SContainer = styled.div<{ visible: boolean }>`
  display: flex;
  ${({ visible }) =>
    visible &&
    css`
      opacity: 0;
      user-select: none;
    `}

  > button {
    flex: 1;
  }
`
