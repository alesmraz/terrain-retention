import { SContainer } from "./ColumnControl.styled"

interface Props {
  isVisible: boolean
  onRowAdd: () => void
  onRowRemove: () => void
}

export function ColumnControl(props: Props) {
  return (
    <SContainer visible={props.isVisible}>
      <button onClick={props.onRowAdd}>+</button>
      <button onClick={props.onRowRemove}>-</button>
    </SContainer>
  )
}
