import styled from "@emotion/styled"

export const SSection = styled.section`
  margin-top: 20px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 20px;
`

export const SButton = styled.button<{ variant: "add" | "remove" }>`
  flex: 1;
  height: 40px;
  border: none;
  background-color: ${({ variant }) =>
    variant === "add" ? "#8bc34a" : "#f44336"};
  color: #fff;
  cursor: pointer;
  transition: background-color 0.1s ease-in-out;

  &:hover,
  &:focus {
    background-color: ${({ variant }) =>
      variant === "add" ? "#7eb53f" : "#b53128"};
  }
`
