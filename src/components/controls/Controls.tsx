import { ROWS_COUNT_MIN } from "../../limits"
import { SSection, SButton } from "./Controls.styled"

interface Props {
  columns: number[]
  setColumns: (value: number[]) => void
}

export function Controls(props: Props) {
  function onColumnAdd() {
    const newColumns = [...props.columns]
    const lastUnsetColumnIndex = newColumns.indexOf(0)

    if (lastUnsetColumnIndex !== -1) {
      newColumns[lastUnsetColumnIndex] = ROWS_COUNT_MIN
      props.setColumns(newColumns)
    }
  }

  function onColumnRemove() {
    const newColumns = [...props.columns]
    let lastSetColumnIndex = -1

    for (let index = newColumns.length - 1; index > 0; index--) {
      const element = newColumns[index]

      if (element !== 0) {
        lastSetColumnIndex = index
        break
      }
    }

    if (lastSetColumnIndex !== -1) {
      newColumns[lastSetColumnIndex] = 0
      props.setColumns(newColumns)
    }
  }

  return (
    <SSection>
      <SButton variant="add" onClick={onColumnAdd}>
        add col
      </SButton>
      <SButton variant="remove" onClick={onColumnRemove}>
        remove col
      </SButton>
    </SSection>
  )
}
